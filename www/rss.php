<?php 
	// How many links to show?
	$limit = 5;
	$display_block = '';
	$rss = new DOMDocument();
	// Enter RSS feed below
	$rss->load('https://www.nasa.gov/rss/dyn/shuttle_station.rss');
	$feed = array();
	//
	foreach ($rss->getElementsByTagName('channel') as $node) {
		
		$display_block.='<h1>'.$node->getElementsByTagName('title')->item(0)->nodeValue.'</h1>';
		$display_block.='<h3>'.$node->getElementsByTagName('description')->item(0)->nodeValue.'</h3>';	
		$display_block.='<p><a href="'.$node->getElementsByTagName('link')->item(0)->nodeValue.'" title"Go to site" target="_NEW">'.$node->getElementsByTagName('link')->item(0)->nodeValue.'</a></p>';				
	}
	//
	foreach ($rss->getElementsByTagName('item') as $node) {
		if(isset($node->getElementsByTagName('link')->item(0)->nodeValue)){
			$item = array ( 
				'title' => $node->getElementsByTagName('title')->item(0)->nodeValue,
				'desc' => $node->getElementsByTagName('description')->item(0)->nodeValue,
				'link' => $node->getElementsByTagName('link')->item(0)->nodeValue,
				'date' => $node->getElementsByTagName('pubDate')->item(0)->nodeValue,
				);
			array_push($feed, $item);
		}
	}
	for($x=0;$x<$limit;$x++) {
		$title = str_replace(' & ', ' &amp; ', $feed[$x]['title']);
		$link = $feed[$x]['link'];
		$description = $feed[$x]['desc'];
		$date = date('l F d, Y', strtotime($feed[$x]['date']));
		$display_block.= '<p><strong><a href="'.$link.'" title="'.$title.'">'.$title.'</a></strong><br />';
		$display_block.= '<small><em>Posted on '.$date.'</em></small><br />';
		$display_block.= ''.$description.'</p>';
	}
	// Echo this where you want it to display
	echo $display_block;
?>